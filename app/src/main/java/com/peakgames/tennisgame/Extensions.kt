package com.peakgames.tennisgame

import com.peakgames.tennisgame.models.Player
import com.peakgames.tennisgame.models.Surface

fun Player.synchronizeExperience() {
    if (this.experience > this.total_experience) this.total_experience = this.total_experience
}

fun Player.calculateExperience(secondPlayer: Player, surface: Surface) {
    if (this.total_experience > secondPlayer.total_experience) {
        this.total_experience += 3
    } else {
        secondPlayer.total_experience += 3
    }
    this.total_experience += 1
    if (this.hand == "left") this.total_experience += 2

    secondPlayer.total_experience += 1
    if (secondPlayer.hand == "left") secondPlayer.total_experience += 2

    when (surface) {
        Surface.CLAY -> if (this.skills.clay > secondPlayer.skills.clay) this.total_experience += 4 else secondPlayer.total_experience += 4
        Surface.GRASS -> if (this.skills.grass > secondPlayer.skills.grass) this.total_experience += 4 else secondPlayer.total_experience += 4
        Surface.HARD -> if (this.skills.hard > secondPlayer.skills.hard) this.total_experience += 4 else secondPlayer.total_experience += 4
    }
}