package com.peakgames.tennisgame

import android.util.Log
import com.peakgames.tennisgame.models.GameType
import com.peakgames.tennisgame.models.Player
import com.peakgames.tennisgame.models.Surface

class Tournament(private val type: GameType, var players: MutableList<Player>, private val surface: Surface) {
    private var tour: Int = 1
    private var fixture: MutableList<Pair<Player, Player>> = mutableListOf()

    init { //Yeni oyunda sıfırlama için
        tour = 1
        fixture.clear()
        players = if (type == GameType.ELIMINATION) {
            startElimination()
        } else {
            startLeague()
        }
    }

    private fun startElimination(): MutableList<Player> {
        val players = players.filter { !it.eliminated }.toMutableList()
        return if (players.size <= 1) {
            this.players
        } else {
            (1..players.size / 2).forEach { _ ->
                val firstPlayer = players.toMutableList().filter { !it.eliminated && it.tour == tour }.random()
                val secondPlayer = (players - firstPlayer).toMutableList().filter { !it.eliminated && it.tour == tour }.random()
                Log.e("$tour. tur, Takımlar", "${firstPlayer.id} vs ${secondPlayer.id}")
                addMatch(firstPlayer, secondPlayer)
                startNewGame(firstPlayer, secondPlayer)
            }
            tour++
            startElimination()
        }
    }

    private fun startLeague(): MutableList<Player> {
        var players = players.toMutableList()
        players.forEach { firstPlayer ->
            players = ((players - firstPlayer).toMutableList())
            players.forEach { secondPlayer ->
                addMatch(firstPlayer, secondPlayer)
            }
        }
        fixture.shuffled().forEachIndexed { _, players ->
            startNewGame(players.first, players.second)
        }
        return this.players
    }

    private fun addMatch(home: Player, guest: Player) {
        home.synchronizeExperience()
        guest.synchronizeExperience()
        home.calculateExperience(guest, surface)
        fixture.add(Pair(home, guest))
    }

    private fun startNewGame(home: Player, guest: Player) {
//        Log.e("maç", "${home.id} vs ${guest.id}")
//        var winnerOfGame: Player? = null
        val twoPlayersTotalExperience = home.total_experience + guest.total_experience
        val firstPlayerWinningProbability = home.total_experience.toDouble().div(twoPlayersTotalExperience.toDouble())
        val secondPlayerWinningProbability = guest.total_experience.toDouble().div(twoPlayersTotalExperience.toDouble())
        when {
            firstPlayerWinningProbability > secondPlayerWinningProbability -> {
                home.tour += 1
//                winnerOfGame = home
                setWinningExperience(guest, home)
            }
            firstPlayerWinningProbability < secondPlayerWinningProbability -> {
                guest.tour += 1
//                winnerOfGame = guest
                setWinningExperience(home, guest)
            }
            else -> { // Berabere kalma durumu
                if (home.experience > guest.experience) {
//                    winnerOfGame = home
                    setWinningExperience(guest, home)
                } else {
//                    winnerOfGame = guest
                    setWinningExperience(home, guest)
                }
            }
        }
//        Log.e("$tour.", winnerOfGame?.toString() ?: "")
    }

    private fun setWinningExperience(
        loosing: Player,
        winning: Player
    ) {
        if (type == GameType.ELIMINATION) {
            loosing.eliminated = true
            winning.total_experience += 20
            loosing.total_experience += 10
        } else {
            winning.total_experience += 10
            loosing.total_experience += 1
        }
    }

}