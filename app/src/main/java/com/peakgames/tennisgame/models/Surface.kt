package com.peakgames.tennisgame.models

enum class Surface{
    CLAY, GRASS, HARD
}