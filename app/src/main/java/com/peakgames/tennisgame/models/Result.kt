package com.peakgames.tennisgame.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder("order", "player_id", "gained_experience", "total_experience")
data class Result(
    val order: Int,
    val player_id: Int,
    val gained_experience: Int,
    val total_experience: Int
)