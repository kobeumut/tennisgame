package com.peakgames.tennisgame.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)

data class Output(
    val results: MutableList<Result>
)