package com.peakgames.tennisgame.models
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Skills(
    val clay: Int,
    val grass: Int,
    val hard: Int
)