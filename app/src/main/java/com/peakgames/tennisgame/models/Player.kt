package com.peakgames.tennisgame.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Player(
    val id: Int,
    val hand: String,
    val experience: Int,
    val skills: Skills,
    var total_experience: Int,
    var eliminated: Boolean = false,
    var tour: Int = 1

)