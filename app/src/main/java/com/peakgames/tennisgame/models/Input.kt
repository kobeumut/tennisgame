package com.peakgames.tennisgame.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Input(
    val players: MutableList<Player>,
    val tournaments: MutableList<Tournament>
)