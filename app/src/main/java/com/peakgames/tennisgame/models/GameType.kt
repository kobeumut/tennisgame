package com.peakgames.tennisgame.models

enum class GameType
{ LEAGUE, ELIMINATION }