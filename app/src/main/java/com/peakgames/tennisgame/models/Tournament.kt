package com.peakgames.tennisgame.models
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Tournament(
    val id: Int,
    val surface: String,
    val type: String
){
    fun getType():GameType{
        return when(type){
            "league" -> GameType.LEAGUE
            "elimination" -> GameType.ELIMINATION
            else -> GameType.ELIMINATION
        }
    }
    fun getSurface():Surface{
        return when(surface){
            "clay" -> Surface.CLAY
            "grass" -> Surface.GRASS
            else -> Surface.HARD
        }
    }
}