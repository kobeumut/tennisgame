package com.peakgames.tennisgame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.peakgames.tennisgame.models.Input
import com.peakgames.tennisgame.models.Output
import com.peakgames.tennisgame.models.Player
import com.peakgames.tennisgame.models.Result
import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val result: MutableList<Result> = mutableListOf()
    private lateinit var playerList: MutableList<Player>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mapper = jacksonObjectMapper()
        val data = mapper.readValue<Input>(resources.openRawResource(R.raw.input))

        playerList = data.players

        data.tournaments.forEachIndexed { index, tournament ->
            val lig = Tournament(tournament.getType(), playerList, tournament.getSurface())
            playerList = lig.players

            if(data.tournaments.size-1 == index){
                playerList.sortWith(compareBy ({ it.total_experience-it.experience }, {it.experience}))
                Log.e("Sonuç", playerList.asReversed().toString())
                playerList.asReversed().forEachIndexed { index, player ->
                    result.add(Result(index+1,player.id,player.total_experience-player.experience,player.total_experience))
                }
                val ow = ObjectMapper().writer().withDefaultPrettyPrinter()
                val json = ow.writeValueAsString(Output(result))
                Log.e("Output JSON", json)
                editText.setText(json)

            }
        }
    }
}
